/**
 * @author Barinov group 15OIT18.
 */
public class Rational {
    private int num;
    private int denum;

    public Rational(int num, int denum) {
        this.num = num;
        this.denum = denum;
    }

    public Rational(int num) {

        this(num, 1);
    }

    public Rational() {
        this(1, 1);
    }

    public int getNum() {


        return num;
    }

    public int getDenum() {


        return denum;
    }

    @Override
    public String toString() {
        return num + "/" + denum;
    }


    public void setNum(int num) {
        this.num = num;
    }

    public void setDenum(int denum) {
        this.denum = denum;
    }

    public Rational summa(Rational rational1) {
        Rational rational2 = new Rational();
        if (this.denum > rational1.denum && this.denum % rational1.denum == 0) {
            rational1.num = (this.denum / rational1.denum) * rational1.num;

            rational2 = new Rational(rational1.num + this.num, this.denum);
            return rational2;
        }
        if (this.denum == rational1.denum) {

            rational2 = new Rational(rational1.num + this.num, this.denum);
            return rational2;
        }
        if (this.denum > rational1.denum && this.denum % rational1.denum != 0) {
            this.num = (this.denum * rational1.denum) / this.denum * this.num;
            rational1.num = (this.denum * rational1.denum) / rational1.denum * rational1.num;

            rational2 = new Rational(rational1.num + this.num, rational1.denum * this.denum);
            return rational2;
        }
        if (rational1.denum > this.denum && rational1.denum % this.denum != 0) {
            this.num = (rational1.denum * this.denum) / this.denum * this.num;
            rational1.num = (rational1.denum * this.denum) / rational1.denum * rational1.num;

            rational2 = new Rational(rational1.num + this.num, rational1.denum * this.denum);
            return rational2;
        }
        if (rational1.denum > this.denum && rational1.denum % this.denum == 0) {
            this.num = (rational1.denum / this.denum) * this.num;

            rational2 = new Rational(this.num + rational1.num, rational1.denum);
            return rational2;

        }
        return rational2;
    }

    public Rational difference(Rational rational1) {
        Rational rational4 = new Rational();
        if (this.denum == rational1.denum) {
            rational4 = new Rational(this.num - rational1.num, this.denum);
            return rational4;
        }
        if (this.denum > rational1.denum && this.denum % rational1.denum != 0) {
            this.num = (this.denum * rational1.denum) / this.denum * this.num;
            rational1.num = (this.denum * rational1.denum) / rational1.denum * rational1.num;

            rational4 = new Rational(this.num - rational1.num, this.denum * rational1.denum);
            return rational4;
        }
        if (rational1.denum > this.denum && rational1.denum % this.denum == 0) {
            this.num = (rational1.denum / this.denum) * this.num;

            rational4 = new Rational(this.num - rational1.num, rational1.denum);
            return rational4;
        }
        if (this.denum > rational1.denum && this.denum % rational1.denum == 0) {
            rational1.num = (this.denum / rational1.denum) * rational1.num;

            rational4 = new Rational(this.num - rational1.num, this.denum);
            return rational4;
        }
        if (rational1.denum > this.denum && rational1.denum % this.denum != 0) {
            this.num = (rational1.denum * this.denum) / this.denum * this.num;
            rational1.num = (rational1.denum * this.denum) / rational1.denum * rational1.num;

            rational4 = new Rational(this.num - rational1.num, rational1.denum * this.denum);
            return rational4;
        }
        return rational4;
    }

    public Rational div(Rational rational1) {
        Rational rational = new Rational(this.num * rational1.denum, this.denum * rational1.num);
        return rational;
    }

    public Rational multiplication(Rational rational2) {
        Rational rational3 = new Rational(this.num * rational2.num, this.denum * rational2.denum);
        return rational3;

    }
}