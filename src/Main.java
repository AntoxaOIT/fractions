import java.io.*;
import java.nio.Buffer;
import java.util.Arrays;
import java.util.Scanner;
import java.util.regex.*;

import static java.lang.System.out;
import static java.lang.System.setOut;

/**
 * @author Barinov group 15OIT18.
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new FileReader("file.txt"));
        String string;
        BufferedWriter file = new BufferedWriter(new FileWriter("file2.txt")); // Создание файла ,при его отсутствии.
        BufferedWriter file1 = new BufferedWriter(new FileWriter("error.txt")); //Файл для некорректно введенных выражений.
        while ((string = bufferedReader.readLine()) != null) { //Читает строки пока они не кончатся
            ischeckup(string); //Проверка строки.

            if (!ischeckup(string)) { //Запись в файл некорректных выражений.
                file1.write(string);
                file1.write("\r\n");
                continue;

            }
            String[] splitArray = string.split(" ");
            String[] n = splitArray[0].split("/");
            String[] n1 = splitArray[2].split("/");
            int number = Integer.parseInt(n[0]);
            int denumber = Integer.parseInt(n[1]);
            int number2 = Integer.parseInt(n1[0]);
            int denumber2 = Integer.parseInt(n1[1]);
            Rational first = new Rational(number, denumber);
            Rational second = new Rational(number2, denumber2);
            file.write(String.valueOf(math(first, second, splitArray[1]))); //Выполнение операций.
            file.append("\n"); // Деление по строкам

        }
        file1.close();
        file.close();
        bufferedReader.close();
    }


    /**
     * public static void main(String[] args) {
     * String[] strings = new String[3];
     * Rational rational = new Rational();
     * Rational rational1 = new Rational();
     * write(strings);
     * splitarray(0, strings, rational);
     * splitarray(2, strings, rational1);
     * System.out.println(rational + " " + strings[1] + " " + " " + rational1 + " = " + math(rational, rational1, strings[1]));
     * <p>
     * /**
     * Метод,в котором вводится строка и который разделяет её на 3 части
     * и отправляет в другой метод для сохранения их в массив .
     *
     * @param strings массив для хранения частей строки
     */

    /**
     * private static void write(String[] strings) {
     * String string;
     * boolean value;
     * do {
     * out.println("Введите исходную строку:");
     * string = scanner.nextLine();
     * value = ischeckup(string);
     * if (!ischeckup(string)) {
     * out.println("Исходные данные введены некорректно, проверьте корректность ввода и введите еще раз:: ");
     * }
     * } while (!ischeckup(string));
     * int i = 0;
     * for (String retval : string.split(" ")) {
     * savearray(i, strings, retval);
     * i++;
     * }
     * }
     */
    private static boolean ischeckup(String string) {
        Pattern pattern = Pattern.compile("[1-9][0-9]*[/][1-9][0-9]*\\s[+-:*]\\s[1-9][0-9]*[/][1-9][0-9]*");
        Matcher matcher = pattern.matcher(string);
        return matcher.find();
    }

    /**
     * Метод, который сохраняет часть разделенной строки в массив.
     *
     * @param i       номер элемента массива
     * @param strings массив
     * @param retval  часть разделенной строки
     */
    private static void savearray(int i, String[] strings, String retval) {
        strings[i] = retval;

    }

    /**
     * Метод, который разделяет строку на числитель и знаменатель и сохраняет их в обьекте Fraction.
     *
     * @param i        номер элемента массива
     * @param strings  массив
     * @param rational обьект
     */
    private static void splitarray(int i, String[] strings, Rational rational) {
        String[] strings1 = new String[2];
        int i1 = 0;
        for (String retval : strings[i].split("/")) {
            savearray(i1, strings1, retval);
            i1++;
        }
        rational.setNum(Integer.parseInt(strings1[0]));
        rational.setDenum(Integer.parseInt(strings1[1]));
    }

    public static Rational math(Rational rational, Rational rational1, String string) {
        Rational rational2 = new Rational();
        switch (string) {
            case "+":
                rational2 = rational.summa(rational1);
                return rational2;
            case "-":
                rational2 = rational.difference(rational1);
                return rational2;
            case ":":
                rational2 = rational.div(rational1);
                return rational2;
            case "*":
                rational2 = rational.multiplication(rational1);
                return rational2;
        }
        return null;
    }


}




